$(function(){
	$( "#dateFrom" ).datepicker({
	  changeMonth: true,
	  changeYear: true,
	  numberOfMonths: 1,
	  dateFormat: 'yy-mm-dd',
	  yearRange: "-20:+0",
	  onClose: function( selectedDate ) {
	    $( "#dateTo" ).datepicker( "option", "minDate", selectedDate );
	  }
	});
	$( "#dateTo" ).datepicker({
	  changeMonth: true,
	  numberOfMonths: 1,
	  changeYear: true,
	  dateFormat: 'yy-mm-dd',
	  onClose: function( selectedDate ) {
	    $( "#dateFrom" ).datepicker( "option", "maxDate", selectedDate );
	  }
	});
	$.get('companies.json', function(data){
	    $(".companyName").typeahead({ 
	    								source:data,
	    								updater: function(e){
	    									$('.companySymbol').val(e.id);
	    									return e;
	    								}
	    							});
	},'json');

	$('.quoteForm').validate({
	});
});