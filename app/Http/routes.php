<?php

Route::get('/', 'QuotesController@historicalQuotes');
Route::get('quotes',['as'=>'home','uses'=>'QuotesController@historicalQuotes']);
Route::post('historicalQuotes', 'QuotesController@getHistoricalQuotes');
Route::get('companies', 'QuotesController@companies');
Route::get('companies.json','QuotesController@getCompaniesJSON');