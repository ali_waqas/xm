<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use Input;
use View;
use App\Http\Requests\StockRequestValidator;
use Cache;
use Redirect;


class QuotesController extends Controller{
	/**
	 * Validation rules for form posted.
	 *
	 * @var array
	 */
	public $stockRequestValidationRules;

	/**
	 * Quotes Instance
	 *
	 * @var \App\Http\Controllers\Controller\Quotes
	 */

	public $quotes;
	
	/**
	 * Constructor prepare prepares the dependancy injection and objects
	 *
	 * @param  string  \App\Http\Controllers\Controller\Quotes $Quotes
	 * @return void
	 */

	public function __construct(Quotes $Quotes){
		$this->quotes = $Quotes;
		$symbols = $this->quotes->getCompanySymbols();
		$this->stockRequestValidationRules = [
												'symbol'=>'required|in:'.$symbols,
												'dateFrom'=>'required|date_format:"'.$this->quotes->ApiDateFormat.'"',
												'dateTo'=>'required|date_format:"'.$this->quotes->ApiDateFormat.'"',
												'email'=>'required|email'
											 ];
	}

	/**
	 * "Get" Home controller, listens the route at '/' and 'quotes' 
	 *
	 * @return string:BladeView
	 */

	public function historicalQuotes(){
		if(Input::has('symbol')){
			$symbol = Input::get('symbol');
			$companies = (array)json_decode(Cache::get('companyListKeyValue'));
			$companyName = $symbol.' - '. $companies[$symbol];
		}else{
			$companyName = '';
			$symbol = '';
		}
		
		$content = View::make('partials.form')->with(['symbol'=>$symbol,'companyName'=>$companyName])->render();
		return view('content',compact('content'));
	}

	/**
	 * Post Conrtoller responsible to handle form request
	 *
	 * @param  string  App\Http\Requests\StockRequestValidator #request
	 * @return string:BladeView
	 */

	public function getHistoricalQuotes(StockRequestValidator $request){

		$validation = Validator::make($request->all(),$this->stockRequestValidationRules);
		if($validation->fails()){
			return Redirect::to('quotes')->withInput($request->all())->withErrors($validation);
		}else{
			$input = $request->all();
			$companies = (array)json_decode(Cache::get('companyListKeyValue'));
			$symbol = $input['symbol'];
			$dateFrom = $input['dateFrom'];
			$dateTo = $input['dateTo'];
			$email = $input['email'];
			$companyName = $companies[$symbol];
			$data = $this->quotes->getCSVData($symbol,$dateFrom,$dateTo);
			$contentViewData = 	[
									'symbol'=>$symbol,
									'companyName'=>$companyName,
									'headers'=>array_shift($data),
									'data'=>$data,
									'dateFrom'=>$dateFrom,
									'dateTo'=>$dateTo
								];
			$content = View::make('partials.dataTable')->with($contentViewData)->render();
			
			$emailViewData = ['companyName'=>'From: '.$dateFrom.' To: '.$dateTo,'symbol'=>$symbol,'emailMessage'=>$contentViewData];
			$emailSubject = $companyName;
			$attachment = null;
			if($this->quotes->sendQuoteEmail($email,$emailSubject,$emailViewData,$attachment))
				return view('content',compact('content'));
			else
				return Redirect::to('quotes')->withInput($request->all())->withErrors(['There was a problem sending email, please try again.']);
		}
	}

	/**
	 * "Get" Companies controller, listens the route at '/companies'
	 *
	 * @return string:BladeView
	 */

	public function companies(){
		$companylist = json_decode($this->quotes->getCompanyList());
		$content = View::make('partials.companies')->with(
															[
																'headers'=>array_shift($companylist),
																'data'=>$companylist
															]
														 )->render();
		return view('content',compact('content'));
	}
	
	/**
	 * "Get" for Ajax call for companies.json
	 *
	 * @return string
	 */

	public function getCompaniesJSON(){
		return Cache::get('companyListKeyValue2');
	}

}