<?php 
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Validator;
use Input;
use View;
use App\Http\Requests\StockRequestValidator;
use Cache;
use Redirect;
use File;
use Illuminate\Mail\Mailer;

class Quotes{
	/**
	 * Yahoo Finanace Data.
	 *
	 * @var array
	 */
	public $data = [];
	/**
	 * Yahoo Finance API Uri.
	 *
	 * @var string
	 */
	public $YahooFinanceUri ="http://ichart.yahoo.com/table.csv?";
	/**
	 * File containing company list.
	 *
	 * @var string
	 */
	public $companyListFile;
	/**
	 * Indicates if cached version of companyList should be used.
	 *
	 * @var bool
	 */
	public $companylistUseCache = true;
	/**
	 * Date format to be used for validation purposes.
	 *
	 * @var string
	 */
	public $ApiDateFormat = "Y-m-d";
	/**
	 * View that will be used to send email data.
	 *
	 * @var string
	 */
	public $quoteEmailView = 'email.quote_email';

	/**
	 * The Swift_Mailer Instance
	 *
	 * @var \Illuminate\Mail\Mailer
	 */

	public $Mailer;

	/**
	 * Constructor prepare prepares the dependancy injection and 
	 *
	 * @param  string  Illuminate\Mail\Mailer $Mailer
	 * @return void
	 */

	public function __construct(Mailer $Mailer){
		$this->Mailer = $Mailer;
		$this->companyListFile = storage_path("resources/companylist.csv");
	}
	
    /**
     * Connects with Yahoo Finance API, fetches the csv and return an array of data.
     *
     * @param  string  $symbol
     * @param  string  $dateFrom
     * @param  string  $dateTo
     * @return array
     */

	public function getCSVData($symbol,$dateFrom,$dateTo){
		if(empty($symbol) || empty($dateFrom) || empty($dateTo))
			throw new \Exception("Unable to to fetch data, one or more require params are missing", 1);
		
		$dateFrom = date_parse_from_format($this->ApiDateFormat, $dateFrom);
		$dateTo = date_parse_from_format($this->ApiDateFormat, $dateTo);
		$dataUri = $this->YahooFinanceUri.'s='.urlencode($symbol).'&a='.($dateFrom['month']-1).'&b='.$dateFrom['day'].'&c='.$dateFrom['year'].'&d='.($dateTo['month']-1).'&e='.$dateTo['day'].'&f='.$dateTo['year'];
		$yahooData = fopen($dataUri, 'r');
		while (($record = fgetcsv($yahooData)) !== FALSE) {
			$this->data[] = $record;
		}

		return $this->data;
	}

	/**
	 * Gets the list of all the companies
	 * @return string
	 */

	public function getCompanyList(){
		if($this->companylistUseCache && Cache::has('companyList')){
			return Cache::get('companyList');
		}else{
			$companies = [];
			$companyListFile = fopen($this->companyListFile,'r');
			
			while (($company = fgetcsv($companyListFile)) !== FALSE) {
				$companies[] = $company;
			}
			$companylist = json_encode($companies);
			Cache::forever('companyList',$companylist);
			return $companylist;
		}
	}

	/**
	 * Reads the data from app/storage/resources/companyList.csv and Generates company symbols
	 * @return string
	 */

	public function getCompanySymbols(){
		
		// Checks the Cache for the companySymbols, if Cache has the key it returns the data from Cache
		// If Cache does not have the required key, it reads the companyList.csv file, 
		// prepares the proper cache keys and return the company symbols.

		if($this->companylistUseCache && Cache::has('acompanySymbols') && Cache::has('companyListKeyValue') && Cache::has('companyListKeyValue2')){
			return Cache::get('companySymbols');
		}else{
			$symbols = [];
			$symbols2 = [];
			$companyListFile = fopen($this->companyListFile,'r');
			
			while (($company = fgetcsv($companyListFile)) !== FALSE) {
				$symbols[$company[0]] = $company[1];
				$symbols2[] = ['id'=>$company[0],'name'=>$company[0].' - '.$company[1]];
			}

			$companySymbols = implode(",",array_keys($symbols));
			Cache::forever('companySymbols',$companySymbols);
			Cache::forever('companyListKeyValue',json_encode($symbols));
			Cache::forever('companyListKeyValue2',json_encode($symbols2));
			return $companySymbols;
		}
	}

	/**
	 * Sends and email to the user with the data Yahoo Finance Data.
	 *
	 * @param  string  $email
	 * @param  string  $subject
	 * @param  array   $viewData
	 * @return bool
	 */

	public function sendQuoteEmail($email,$subject,$viewData){
		if(empty($email) || !is_array($viewData))
			throw new Exception("Unable to send email, please check email params", 1);
			
			// Prepares an HTML document with the view from views.email.historical_date, saves it in app/storage/tmp

			$emailView = View::make('email.historical_data')->with($viewData['emailMessage'])->render();		
			$tmpFileName = $subject.'('.$viewData['symbol'].') '.$viewData['companyName'].'.html';
			$fileStoragePath = storage_path('tmp');
			$attachment = $fileStoragePath.'/'.$tmpFileName;
			// Send an email to specified email.
			if(File::put($attachment,$emailView)){
				$mail = $this->Mailer->send($this->quoteEmailView, $viewData , function($message) use($email,$subject,$attachment){
			    	$message->to($email, 'Dear Customer')->subject($subject);
			    	if($attachment!=null && File::exists($attachment))
			    	$message->attach($attachment);
				});
			}

			// If email does not have any failure, delete the tmp file, and return true
			if(count($this->Mailer->failures())==0){
				File::delete($attachment);
				return true;
			}else
				return false;
	}

}