<div class="jumbotron">
  @if($errors->any())
    <div class="alert alert-danger" role="alert">
      <a href="#" class="alert-link"></a>
      @foreach($errors->all() as $error)
        <p>{{$error}}</p>
      @endforeach
    </div>
  @endif
  <h3 style="text-align: center; color: #337AB7">{!! $companyName !!} <small class="text-primary">({!! $symbol !!})</small></h3>
  <h3>Historical Prices <small style="color: #999; font-size: 14px;">({{$dateFrom}} to {{$dateTo}})</small></h3>   
  <div class="row">
    <div class="col-md-12">
      <form method="post" action="{{URL::to('historicalQuotes')}}" autocomplete="off" class="quoteForm form-inline">
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
        <input type="hidden" class="form-control companySymbol required" id="symbol" name="symbol" value="{{Input::get('symbol')}}">
        <input type="hidden" class="form-control companyName required" id="companyName" placeholder="Company" name="companyName" value="{{Input::get('companyName')}}">
        <input type="hidden" class="form-control required" id="Email" placeholder="Email" name="email" value="{{Input::get('email')}}">
        <div class="form-group">
          <label for="dateFrom">Start Date</label>
          <input type="text" class="form-control required" id="dateFrom" placeholder="2014-01-01" name="dateFrom" value="{{Input::get('dateFrom')}}">
          <label for="dateTo">End Date</label>
          <input type="text" class="form-control required" id="dateTo" placeholder="{{date('Y-m-d',time())}}" name="dateTo" value="{{Input::get('dateTo')}}">
        </div>
        <button type="submit" class="btn btn-default pull-right">Submit</button>
      </form>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-12">
    <div class="pull-right"><a href="javascript:;" class="hide_show_chart"><span class="glyphicon glyphicon-chevron-up hide_chart_icon" aria-hidden="true"></span> Hide Chart</a></div>
  </div>
</div>

<div class="row chart">
  <div class="col-md-12">
    <div class="chart_div" id="chart_div" style="width: 100%; height: 400px;">

    </div>
  </div>
</div>

<div class="row" style="margin-top: 50px">
  <div class="col-md-12">
    <table class="table table-hover table-responsive">
      <thead>
      	<tr>
      		<th>{{$headers[0]}}</th>
      		<th>{{$headers[1]}}</th>
      		<th>{{$headers[2]}}</th>
      		<th>{{$headers[3]}}</th>
      		<th>{{$headers[4]}}</th>
      		<th>{{$headers[5]}}</th>
      		<th>{{$headers[6]}}</th>
      	</tr>
      </thead>
      <tbody>
        @if(count($data)>0)
      	  @foreach($data as $row)
        	  	<tr class="dataRow" data-date={{strtotime($row[0])}} data-open="{{number_format((float)$row[1], 2, '.', '')}}" data-high="{{number_format((float)$row[2], 2, '.', '')}}" data-low="{{number_format((float)$row[3], 2, '.', '')}}" data-close="{{number_format((float)$row[4], 2, '.', '')}}" data-close="{{number_format((float)$row[4], 2, '.', '')}}" data-volume="{{$row[5]}}" >
          	  	<td >{{date("M d, Y",strtotime($row[0]))}}</td>
          	  	<td>{{number_format((float)$row[1], 2, '.', '')}}</td>
          			<td>{{number_format((float)$row[2], 2, '.', '')}}</td>
          			<td>{{number_format((float)$row[3], 2, '.', '')}}</td>
          			<td>{{number_format((float)$row[4], 2, '.', '')}}</td>
          			<td>{{$row[5]}}</td>
          			<td>{{number_format((float)$row[6], 2, '.', '')}}</td>
        	  	</tr>
      	  @endforeach
        @else
          <tr><td colspan="7"><center>No data available</center></td></tr>
        @endif
      </tbody>
    </table>
  </div>
</div>
