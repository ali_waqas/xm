@if($errors->any())
	<div class="alert alert-danger" role="alert">
	  <a href="#" class="alert-link"></a>
	  @foreach($errors->all() as $error)
	  	<p>{{$error}}</p>
	  @endforeach
	</div>
@endif
<form method="post" action="{{URL::to('historicalQuotes')}}" autocomplete="off" class="quoteForm">
<input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
<input type="hidden" class="form-control companySymbol required" id="symbol" name="symbol" value="{{(!empty(Input::old('symbol')))?Input::old('symbol'):$symbol}}">
  <div class="form-group">
    <label for="symbol">Company Symbol</label>
    <input type="text" class="form-control companyName required" id="companyName" placeholder="Company" name="companyName" value="{{(!empty(Input::old('companyName')))?Input::old('companyName'):$companyName}}">
  </div>
  <div class="form-group">
    <label for="dateFrom">Start Date</label>
    <input type="text" class="form-control required" id="dateFrom" placeholder="2014-01-01" name="dateFrom" value="{{Input::old('dateFrom')}}">
  </div>
  <div class="form-group">
    <label for="dateTo">End Date</label>
    <input type="text" class="form-control required" id="dateTo" placeholder="{{date('Y-m-d',time())}}" name="dateTo" value="{{Input::old('dateTo')}}">
  </div>
  <div class="form-group">
    <label for="exampleInputPassword1">Email</label>
    <input type="email" class="form-control required" id="Email" placeholder="Email" name="email" value="{{Input::old('email')}}">
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
</form>