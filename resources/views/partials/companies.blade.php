<h1>Companies</h1>
<table class="table table-hover table-responsive">
  <thead>
  	<tr>
    	<th>Company</td>
    	<th>{{$headers[2]}}</th>
  		<th>{{$headers[3]}}</th>
  		<th>{{$headers[4]}}</th>
  		<th>{{$headers[5]}}</th>
  		<th>{{$headers[6]}}</th>
      <th></th>
  	</tr>
  </thead>
  <tbody>
	  @foreach($data as $row)
  	  	<tr>
    	  	<td>
              <h5>{{$row[0]}} <div><small class="text-primary">{{$row[1]}}</small></div></h5>
          </td>
    			<td>{{$row[2]}}</td>
    			<td>{{$row[3]}}</td>
    			<td>{{$row[4]}}</td>
    			<td>{{$row[5]}}</td>
    			<td>{{$row[6]}}</td>
          <td><a href="{{URL::to('quotes?symbol='.$row[0])}}" class="btn btn-info viewHistoricData">View Historic Data</a></td>
  	  	</tr>
	  @endforeach
  </tbody>
</table>