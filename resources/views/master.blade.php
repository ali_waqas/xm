<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>XM</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="{{asset('assets/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('assets/css/jquery-ui.css')}}">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
            .error{
                color: red;
                font-size: 13px;
                font-weight: normal;
                font-family: 'Arial'
            }
        </style>
    </head>
    <body>

    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <a class="navbar-brand" href="{{URL::to('/')}}">XM Quotes</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <div class="navbar-form navbar-right" style="margin-top: 12px;">
                <a style="color: #ddd; font-size: 16px;" href="{{URL::to('companies')}}">Companies</a>
            </div>
        </div><!--/.navbar-collapse -->
      </div>
    </nav>

    <div class="container" style="margin-top: 60px">
        @yield('content')
    </div>

    <div class="container" style="margin-top: 100px;">
      <footer>
        <p>&copy; XM {{date('Y')}}</p>
      </footer>
    </div> <!-- /container -->       
        <script src="{{asset('assets/js/jquery-1.11.2.min.js')}}"></script>
        <script src="{{asset('assets/js/jquery-ui.js')}}"></script>
        <script src="{{asset('assets/js/jquery.validate.js')}}"></script>
        <script src="{{asset('assets/js/bootstrap.min.js')}}"></script>
        <script src="{{asset('assets/js/typehead.js')}}"></script>
        <script src="{{asset('assets/js/main.js')}}"></script>
        <script src="http://code.highcharts.com/stock/highstock.js"></script>
        <script src="http://code.highcharts.com/stock/modules/exporting.js"></script>

        <script type="text/javascript">
            $(function () {
                // split the data set into ohlc and volume
                var ohlc = [],
                    volume = [],
                    // set the allowed units for data grouping
                    groupingUnits = [[
                        'week',                         // unit name
                        [1]                             // allowed multiples
                    ], [
                        'month',
                        [1, 2, 3, 4, 6]
                    ]],

                    i = 0;
                $.each($('.dataRow'),function(){
                    ohlc.push([
                        $(this).data('date')*1000, // the date 
                        parseFloat($(this).data('open')), // open
                        parseFloat($(this).data('high')), // high
                        parseFloat($(this).data('low')), // low
                        parseFloat($(this).data('close')) // close
                    ]);
                    volume.push([
                        $(this).data('date')*1000, // the date
                        parseInt($(this).data('volume')) // the volume
                    ]);
                });

                $('#chart_div').highcharts('StockChart', {

                    rangeSelector: {
                        selected: 1
                    },

                    title: {
                        text: 'Historical Prices'
                    },

                    yAxis: [{
                        labels: {
                            align: 'right',
                            x: -3
                        },
                        title: {
                            text: 'OHLC'
                        },
                        height: '60%',
                        lineWidth: 2
                    }, {
                        labels: {
                            align: 'right',
                            x: -3
                        },
                        title: {
                            text: 'Volume'
                        },
                        top: '65%',
                        height: '35%',
                        offset: 0,
                        lineWidth: 2
                    }],

                    series: [{
                        type: 'candlestick',
                        name: 'AAPL',
                        data: ohlc.reverse(),
                        dataGrouping: {
                            units: groupingUnits
                        }
                    }, {
                        type: 'column',
                        name: 'Volume',
                        data: volume.reverse(),
                        yAxis: 1,
                        dataGrouping: {
                            units: groupingUnits
                        }
                    }]
                });
                
                $('.hide_show_chart').click(function(){
                    $('.chart').slideToggle('slow',function(){
                        if ($(this).is(':visible')) {
                             $('.hide_show_chart').html('<span class="glyphicon glyphicon-chevron-up" aria-hidden="true"></span> Hide Chart');         
                        } else {
                             $('.hide_show_chart').html('<span class="glyphicon glyphicon-chevron-down" aria-hidden="true"></span> Show Chart');
                        }       
                    });
                });
            
            });
              function drawChart() {

                var dataArray = [];
                $.each($('.dataRow'),function(){
                    dataArray.push([$(this).data('date'),parseFloat($(this).data('open')),parseFloat($(this).data('close'))]);
                });
                dataArray.push(['Date', 'Open', 'Close']);
                var data = google.visualization.arrayToDataTable(dataArray.reverse());

                var options = {
                  title: 'Historical Prices',
                  hAxis: {title: 'Date',  titleTextStyle: {color: '#333'}},
                  vAxis: {minValue: 0}
                };

                var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                chart.draw(data, options);
              }
            </script>

    </body>
</html>
