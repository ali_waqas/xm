<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<style type="text/css">
			#outlook a{padding:0;}
			body{width:100% !important; font-family: 'Arial'; background-color:#41849a;-webkit-text-size-adjust:none; -ms-text-size-adjust:none;margin:0 !important; padding:0 !important;}  
			.ReadMsgBody{width:100%;} 
			.ExternalClass{width:100%;}
			ol li {margin-bottom:15px;}
			img{height:auto; line-height:100%; outline:none; text-decoration:none;}
			#backgroundTable{height:100% !important; margin:0; padding:0; width:100% !important;}
			p {margin: 1em 0;}
			h1, h2, h3, h4, h5, h6 {color:#222222; font-family:Arial, Helvetica, sans-serif; line-height: 100% !important;}
			table td {border-collapse:collapse; ; font-family: 'Arial'}
			.yshortcuts, .yshortcuts a, .yshortcuts a:link,.yshortcuts a:visited, .yshortcuts a:hover, .yshortcuts a span { color: black; text-decoration: none !important; border-bottom: none !important; background: none !important;}
			.im {color:black;}
			div[id="tablewrap"] {
					width:100%; 
					max-width:800px!important;
				}
			table[class="fulltable"], td[class="fulltd"] {
					max-width:100% !important;
					width:100% !important;
					height:auto !important;
				}
						
			#quotesTable tr:hover{
				background-color: #f7f7f7;
				color: #333;
				border-bottom: 1px solid red;
			}
			#quotesTable td{
				color: #999;
				font-weight: normal;
				font-size: 92%;
				text-align: center;
			}
		</style>
	</head>
	</style>
	<body style="width:100% !important; margin:0 !important; padding:0 !important; -webkit-text-size-adjust:none; -ms-text-size-adjust:none; background-color:#f7f7f7;">
		<table cellpadding="0" cellspacing="0" border="0" id="backgroundTable" style="height:auto !important; margin:0; padding:0; width:100% !important; background-color:#f7f7f7; color:#222222;">
			<tr>
				<td>
		         	<div id="tablewrap" style="width:100% !important; max-width:800px !important; text-align:center !important; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important;">
				    	<table id="contenttable" width="800" align="center" cellpadding="0" cellspacing="0" border="0" style="background-color:#f7f7f7; text-align:center !important; margin-top:0 !important; margin-right: auto !important; margin-bottom:0 !important; margin-left: auto !important; border:none; width: 100% !important; max-width:800px !important;">
		            		<tr>
		                		<td width="100%">
		                   			<table bgcolor="#FFFFFF" border="0" cellspacing="0" cellpadding="25" width="800" style="border: 1px solid #ddd" id="quotesTable">
		                        		<tr>
		                            		<td width="100%" bgcolor="#ffffff" style="text-align:left;">
		                            			@yield('content')
		                            		</td>
		                        		</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
				</td>
			</tr>
		</table>
	</body>
</html>
	