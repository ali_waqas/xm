<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class StocksTest extends TestCase{

    public function setup(){
        parent::setUp(); // Don't forget this!
        Session::start();
    }

    public function test_run_prerequisites(){
        if(!File::exists(storage_path("tmp")))
            File::makeDirectory(storage_path("tmp"));

        $this->assertTrue(File::exists(storage_path("tmp")));
        $this->assertTrue(File::exists(storage_path("resources/companylist.csv")));
    }
    
    public function test_companies(){
        $response = $this->call('GET','companies');
        $this->assertViewHas('content');
        $this->assertResponseOk();
    }

    public function test_displays_get_historical_data_form_without_symbol(){
        $response = $this->call('GET','/');
        $this->assertResponseOk();
    }

    public function test_displays_get_historical_data_form_with_symbol(){
        $response = $this->call('GET','/quotes?symbol=GOOG');
        $this->assertResponseOk();
        $this->assertViewHas('content');
    }

    public function test_Form_Validation_Csrf_Missing(){
        $response = $this->call('POST','historicalQuotes',[]);
        $this->assertResponseStatus(500);
    }

    public function test_Form_Validation(){
        $response = $this->call('POST','historicalQuotes',['_token'=>Session::token(),'symbol'=>'GOOG']);
        $this->assertResponseStatus(302);
        $this->assertSessionHasErrors();
        $this->assertRedirectedToRoute('home',[],['errors']); 
    }

    public function test_Form_Validation_Missing_DateTo_And_Email(){
        $response = $this->call('POST','historicalQuotes',['_token'=>Session::token(),'symbol'=>'GOOG','dateFrom'=>'2014-01-01']);
        $this->assertResponseStatus(302);
        $this->assertSessionHasErrors();
        $this->assertRedirectedToRoute('home',[],['errors']);
    }

    public function test_Form_Validation_Missing_Email(){
        $response = $this->call('POST','historicalQuotes',['_token'=>Session::token(),'symbol'=>'GOOG','dateFrom'=>'2014-01-01','dateTo'=>'2015-01-01']);
        $this->assertResponseStatus(302);
        $this->assertSessionHasErrors();
        $this->assertRedirectedToRoute('home',[],['errors']);
    }

    public function testForm_Validation_Invalid_Email(){
        $response = $this->call('POST','historicalQuotes',['_token'=>Session::token(),'symbol'=>'GOOG','dateFrom'=>'2014-01-01','dateTo'=>'2015-01-01','email'=>'emailexample']);
        $this->assertResponseStatus(302);
        $this->assertSessionHasErrors();
        $this->assertRedirectedToRoute('home',[],['errors']);
    }

    public function test_Form_Validation_Invalid_Date_Format(){
        $response = $this->call('POST','historicalQuotes',['_token'=>Session::token(),'symbol'=>'GOOG','dateFrom'=>'01-01-2014','dateTo'=>'2015-01-01','email'=>'email@example.com']);
        $this->assertResponseStatus(302);
        $this->assertRedirectedToRoute('home',[],['errors']);
    }

    public function test_Form_Validation_Invalid_Symbol(){
        $response = $this->call('POST','historicalQuotes',['_token'=>Session::token(),'symbol'=>'GOOGLEFAKE','dateFrom'=>'2014-01-01','dateTo'=>'2015-01-01','email'=>'email@example.com']);
        $this->assertResponseStatus(302);
        $this->assertRedirectedToRoute('home',[],['errors']);
    }

    public function test_GetQuotes_All_Green(){
        $response = $this->call('POST','historicalQuotes',['_token'=>Session::token(),'symbol'=>'GOOG','dateFrom'=>'2014-01-01','dateTo'=>'2015-01-01','email'=>'alee.waqas@yahoo.com']);
        $this->assertResponseOk();
        $this->assertViewHas('content');
    }

}
